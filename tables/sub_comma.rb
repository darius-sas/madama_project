Dir.entries('.').each do |e|
	next if not e =~ /\.csv/
	lines = []
	File.open(e, "r") { |file|
		file.each_line do |line|
			lines << line.split('.').join(',')
		end  
	}
	File.open(e, "w") { |io|
		lines.each do |line|
			io << line
		end
	}
end