from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^board', views.dashboard, name='dashboard'),
    url(r'^ajax/activity$', views.get_activity, name='activity'),
    url(r'^ajax/authorities$', views.get_authorities, name='authorities'),
    url(r'^ajax/degree-distr$', views.get_degree_distr, name='degree-distr'),
    url(r'^ajax/graph-metrics$', views.get_community_and_metrics, name='comm-and-metrics')
]