from django.shortcuts import render
from .mdmgraph import ResearchQuestions as rsq
from .forms import UploadGmlFileForm
from .utils import *
from dashboard import settings
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string
from operator import itemgetter
# Create your views here.


def index(request):
    if request.method == 'POST':
        form = UploadGmlFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_name = '.'.join([random_string(10), 'gml'])
            request.session['gml_file_name'] = file_name
            request.session['original_gml_file_name'] = request.FILES['file'].name
            save_uploaded_file(request.FILES['file'], os.path.join(settings.GML_FILES_FOLDER, file_name))
            return HttpResponseRedirect('/dash/board')
        else:
            return HttpResponse('Error')
    else:
        form = UploadGmlFileForm()
        return render(request, 'dboard/index.html', {'upload_form': form})


def dashboard(request):
    original_file_name = request.session['original_gml_file_name']
    request.session['rq'] = None
    return render(request, 'dboard/dashboard.html', {'file_name': original_file_name})


def get_rsq(request):
    rq = request.session['rq']
    if rq is None:
        file_name = request.session['gml_file_name']
        path = os.path.join(settings.GML_FILES_FOLDER, file_name)
        rq = rsq.RSQ1(path)
        request.session['rq'] = rq
    return rq


def get_activity(request):
    rq = get_rsq(request)
    tweets_per_sec = rq.plot_tweets_per_second_heatmap()
    tweets_per_sec_in_percentage = rq.plot_tweets_per_sec_percentage()
    cumulated_total_tweets = rq.plot_cumulative_tweets()
    plots = [('Tweets/sec', tweets_per_sec),
             ('Tweets/sec in %', tweets_per_sec_in_percentage),
             ('Cumulative', cumulated_total_tweets)]
    context = {'plots': plots, 'title': 'Activity statistics', 'ids': 'activity'}
    html = render_to_string('dboard/activity.html', context)
    return JsonResponse({'plot': html})


def get_authorities(request):
    rq = get_rsq(request)
    rq.compute_metrics()
    svg_filename = request.session['gml_file_name'].split('.')[-2] + '.svg'
    svg_filename = os.path.join(settings.GML_FILES_FOLDER, svg_filename)
    auth = rq.get_authorities_attributes(svg_filename=svg_filename)
    svg = ''
    with open(svg_filename) as svgfile:
        for l in svgfile.readlines():
            svg += l

    header_names = ['Name', 'In degree', 'Out degree', 'PageRank', 'K. Authority', 'K. Hub', 'Avg. Connectivity']

    tables = [('All', sorted(auth['all'], key=itemgetter('name'))),
              ('Retweets', sorted(auth['rt'], key=itemgetter('name'))),
              ('Replies', sorted(auth['re'], key=itemgetter('name'))),
              ('Mentions', sorted(auth['mt'], key=itemgetter('name')))]
    context = {'tables': tables, 'headers': header_names, 'title': 'Authorities list'}
    html = render_to_string('dboard/authorities.html', context)
    html_svg = render_to_string('dboard/auth-svg.html', {'svg': svg, 'title': 'Authorities plot'})
    return JsonResponse({'auth': html, 'svg': html_svg})


def get_degree_distr(request):
    rq = get_rsq(request)
    plots = rq.plot_in_out_degree_distr()
    plots = [('All', plots[0]),
             ('Retweets', plots[1]),
             ('Replies', plots[2]),
             ('Mentions', plots[3])]
    context = {'plots': plots, 'title': 'Degree distribution', 'ids': 'metrics'}
    html = render_to_string('dboard/activity.html', context)
    return JsonResponse({'plot': html})

def get_community_and_metrics(request):
    rq = get_rsq(request)
    rq.community_detection_infomap()
    header_names = ['Graph', 'Assortativity', 'Communities (infomap)', 'Nodes', 'Edges', 'Clustering coeff.', 'First tweet', 'Last tweet']
    table = rq.calculate_diff_table_graph()
    for i,row in enumerate(table):
        for j,e in enumerate(table[i]):
            if type(table[i][j]) is float:
                table[i][j] = "{0:.5f}".format(table[i][j])

    table = [('All', table[1]),
             ('Retweets', table[2]),
             ('Replies', table[4]),
             ('Mentions', table[3])]
    context = {'table': table, 'headers': header_names, 'title': 'General metrics'}
    html = render_to_string('dboard/graph-metrics.html', context)
    return JsonResponse({'table': html})
