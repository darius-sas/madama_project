import os, random, string


def save_uploaded_file(file, path):
    """
    Saves the given uploaded file
    :param file: the uploaded file object
    :param path: the path were to save the uploaded file
    :return:
    """
    with open(path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)


def random_string(N=10):
    """
    Returns a random string of the given length.
    :param N: an integer representing the length of the returned random string.
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


def get_table(list_of_dicts, keys):
    """
    Returns a table from the given list of dicts, using values as columns.
    """
    table = []
    for auth in list_of_dicts:
        row = []
        for k in keys:
            row.append(auth[k])
        table.append(row)
    return table