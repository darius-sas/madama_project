from django import forms


class UploadGmlFileForm(forms.Form):
    file = forms.FileField()