import operator
import numpy
import pytz
import scipy as sp
from ..mdmgraph.Analysis import *
from ..mdmgraph.GraphWrapper import GraphWrapper
from ..mdmgraph import Plot as plt
from ..mdmgraph import Analysis as an


class ResearchQuestions:
    def __init__(self, dataset_name):
        self.graph_wrapper = GraphWrapper()
        self.graph_wrapper.load_gml(dataset_name)

    def compute_metrics(self):
        self.graph_wrapper.compute_centrality_measures()
        self.graph_wrapper.compute_pagerank()
        self.graph_wrapper.compute_authority_score()
        self.graph_wrapper.compute_vertex_connectivity()
        # self.graph_wrapper.compute_local_clustering_coefficient()
        #self.graph_wrapper.compute_local_clustering_coefficient()
        #self.graph_wrapper.compute_edge_betweenness()
        #self.graph_wrapper.load_followers('../dataset/followers.gml')

class RSQ1(ResearchQuestions):
    def __init__(self, path):
        super(RSQ1, self).__init__(path)

    def plot_in_out_degree_distr(self):
        """
        Plot the degree distribution as a line chart for the net and its subnets divided by category
        """
        self.graph_wrapper.divide_combined_net()
        graphs = [self.graph_wrapper, GraphWrapper(self.graph_wrapper.rt_graph), GraphWrapper(self.graph_wrapper.re_graph),
                  GraphWrapper(self.graph_wrapper.mt_graph)]

        i = -1
        result = []
        for g in graphs:
            in_degree_dict = g.get_degree_frequency(mode='in')
            out_degree_dict = g.get_degree_frequency(mode='out')
            degree_in = []
            freq_in = []
            for key in sorted(in_degree_dict):
                degree_in.append(key)
                freq_in.append(in_degree_dict[key])

            degree_out = []
            freq_out = []
            for key in sorted(out_degree_dict):
                degree_out.append(key)
                freq_out.append(out_degree_dict[key])

            series = [[degree_in, freq_in, 'lines', 'IN degree'],
                      [degree_out, freq_out, 'lines', 'OUT degree']]
            i += 1
            result.append(plt.plot_lines_series(series=series, zoom_range_x=[0, 50],
                                                zoom_range_y=[0, 11000]))
        return result

    def get_tweets_frequency(self):
        """
        Calculates the tweets frequency at each timestamp.
        :return: A tuple where the first element are the unique timestamps of the graph, the second element is a
        dictionary with the following keys 'all', 'mt', 're', 'rt' and values
        are the number of edges at each unique timestamp.
        """
        g = self.graph_wrapper.graph
        self.graph_wrapper.divide_combined_net()
        re = self.graph_wrapper.re_graph
        rt = self.graph_wrapper.rt_graph
        mt = self.graph_wrapper.mt_graph

        timezone = pytz.timezone('Europe/Rome')
        convert = lambda t: datetime.datetime.fromtimestamp(t, tz=timezone).strftime('%d/%m/%Y %H:%M')

        all_timestamp = [convert(t) for t in g.es['timestamp']]
        re_timestamp = [convert(t) for t in re.es['timestamp']]
        rt_timestamp = [convert(t) for t in rt.es['timestamp']]
        mt_timestamp = [convert(t) for t in mt.es['timestamp']]

        all_timestamp.sort()
        re_timestamp.sort()
        rt_timestamp.sort()
        mt_timestamp.sort()

        all_dict = {}
        re_dict = {}
        rt_dict = {}
        mt_dict = {}

        for t in all_timestamp:
            all_dict[t] = 0
            re_dict[t] = 0
            mt_dict[t] = 0
            rt_dict[t] = 0

        for t in all_timestamp:
            all_dict[t] += 1

        for t in re_timestamp:
            re_dict[t] += 1

        for t in rt_timestamp:
            rt_dict[t] += 1

        for t in mt_timestamp:
            mt_dict[t] += 1

        all_values = []
        for c in sorted(all_dict.keys()):
            all_values.append(all_dict[c])

        re_values = []
        for c in sorted(re_dict.keys()):
            re_values.append(re_dict[c])

        rt_values = []
        for c in sorted(rt_dict.keys()):
            rt_values.append(rt_dict[c])

        mt_values = []
        for c in sorted(mt_dict.keys()):
            mt_values.append(mt_dict[c])

        frequencies = {'re': re_values, 'rt': rt_values,
                  'mt': mt_values, 'all': all_values}

        unique_timestamps = sorted(list(all_dict.keys()))

        return unique_timestamps, frequencies

    def get_cumulative_tweets(self):
        """
        Calculates the cumulative tweets per second.
        :return: A tuple where the first element represents the timestamps, and the second is a dictionary
        with the following keys 'all', 'rt', 're', 'mt', values is a the number of tweets at each timestamp, ordered
        to match the timestamps of the first tuple element.
        """
        timestamps, frequencies = self.get_tweets_frequency()
        data = frequencies['all']
        all_tweets = [data[0]]*len(data)

        re = frequencies['re']
        re_interact = [re[0]]*len(re)

        rt = frequencies['rt']
        rt_interact = [rt[0]]*len(rt)

        mt = frequencies['mt']
        mt_interact = [mt[0]]*len(mt)

        for i in range(1, len(data)):
            all_tweets[i] = all_tweets[i-1] + data[i]
        for i in range(1, len(re)):
            re_interact[i] = re_interact[i-1] + re[i]
        for i in range(1, len(rt)):
            rt_interact[i] = rt_interact[i-1] + rt[i]
        for i in range(1, len(mt)):
            mt_interact[i] = mt_interact[i-1] + mt[i]

        return timestamps, {'all': all_tweets, 're': re_interact, 'rt': rt_interact, 'mt': mt_interact}

    def plot_tweets_per_second_heatmap(self):
        """
        Create heatmap tweets per minute
        """
        timestamps, frequencies = self.get_tweets_frequency()
        series = [frequencies['re'], frequencies['rt'], frequencies['mt'], frequencies['all']]
        return plt.plot_heatmap('Earth', timestamps, ['re', 'rt', 'mt', 'all'], *series)

    def get_highest_activity_timestamps(self, timestamp_start=0000000000, timestamp_end=9999999999, measure='s', convert=True):
        """
        Returns the timestamps with the highest increase in tweets per second. A timestamp interval can be specified
        with a given precision.
        :param timestamp_start: the starting timestamp.
        :param timestamp_end: the ending timestamp.
        :param measure: the width of the interval to consider; 's'=second, 'm'=minute, 'h'=hour, 'd'=day.
        :param convert: whether to convert or not the timestamp to date format or not.
        :return: a list with the timestamps with the maximum increase in tweets per second.
        """
        g = self.graph_wrapper
        sequence = g.get_interval_edge_number(timestamp_start=timestamp_start, timestamp_end=timestamp_end, measure=measure, convert=convert)

        values = []
        for val in sequence.values():
            values.append(val)

        # calculate derivative vector
        deriv = sp.diff(values)

        # select highest indexes
        highest_index = numpy.where(deriv == max(deriv))

        # create the keys list
        keys_list = list(sequence.keys())

        # select the timestamps that correspond to the high index
        timestamps = []
        for high_index in highest_index[0]:
            timestamps.append(keys_list[high_index])

        return timestamps

    def plot_cumulative_tweets(self):
        """
        Plots the cumulative tweets at each timestamp present in the graph.
        """
        timestamps, frequencies = self.get_cumulative_tweets()
        series = [[timestamps, frequencies['all'], "lines", "all"],
                  [timestamps, frequencies['re'], "lines", "re"],
                  [timestamps, frequencies['rt'], "lines", "rt"],
                  [timestamps, frequencies['mt'], "lines", "mt"]]

        return plt.plot_lines_series(series)

    def plot_tweets_per_sec_percentage(self):
        """
        Plots the number of tweets, of each type, twitted per second relative
        to the total number of tweets (for each type).
        """
        timestamps, frequencies = self.get_tweets_frequency()
        all_d = frequencies['all']
        re_d = frequencies['re']
        rt_d = frequencies['rt']
        mt_d = frequencies['mt']

        for i in range(0, len(all_d)):
            all_d[i] = (all_d[i]/len(self.graph_wrapper.graph.es))*100
            re_d[i] = (re_d[i]/len(self.graph_wrapper.graph.es))*100
            rt_d[i] = (rt_d[i]/len(self.graph_wrapper.graph.es))*100
            mt_d[i] = (mt_d[i]/len(self.graph_wrapper.graph.es))*100

        series = []
        for t in ['all', 'rt', 're', 'mt']:
            series.append([timestamps, frequencies[t], "lines", "percentage " + t])

        return plt.plot_lines_series(series)

    def get_authorities(self):
        """
        Calculates the autorities for the current instance graph object and returns a dictionary containing
        4 subgraphs, 1 subraph for each subnetwork and a subgraph for the combined graph.
        :return: A dictionary where the keys are 'all', 'mt', 'rt', 're', and the values are the corresponding
        autorities subgraphs.
        """
        vertices = self.graph_wrapper.graph.vs

        in_degree = {}
        page_rank = {}
        authority_score = {}
        for v in vertices:
            in_degree[v['id']] = v['in_degree']
            page_rank[v['id']] = v['pagerank']
            authority_score[v['id']] = v['kl_auth']

        sorted_in_degree = sorted(in_degree.items(), key=operator.itemgetter(1), reverse=True)
        best_10_indegree = sorted_in_degree[0: 10]

        sorted_page_rank = sorted(page_rank.items(), key=operator.itemgetter(1), reverse=True)
        best_10_page_rank = sorted_page_rank[0: 10]

        sorted_auth_score = sorted(authority_score.items(), key=operator.itemgetter(1), reverse=True)
        best_10_auth_score = sorted_auth_score[0: 10]

        vertices_ids = []

        for i in range(0, len(best_10_indegree)):
            vertices_ids.append(int(best_10_indegree[i][0]))
            vertices_ids.append(int(best_10_page_rank[i][0]))
            vertices_ids.append(int(best_10_auth_score[i][0]))
        vertices_ids = set(vertices_ids)

        # confronta autorities nelle sottoreti
        self.graph_wrapper.divide_combined_net()
        g_rt = GraphWrapper(self.graph_wrapper.rt_graph)
        g_rt.graph['name'] = 'Retweet Graph'
        g_re = GraphWrapper(self.graph_wrapper.re_graph)
        g_re.graph['name'] = 'Reply Graph'
        g_mt = GraphWrapper(self.graph_wrapper.mt_graph)
        g_mt.graph['name'] = 'Mention Graph'
        subgraphs = {'rt': g_rt, 're': g_re, 'mt': g_mt}

        for g in subgraphs.values():
            g.compute_centrality_measures()
            g.compute_pagerank()
            g.compute_authority_score()
            g.compute_vertex_connectivity()
            g.compute_local_clustering_coefficient()

        self.graph_wrapper.graph['name'] = 'All Graph'
        subgraphs['all'] = self.graph_wrapper

        return subgraphs, vertices_ids

    def get_authorities_attributes(self, svg_filename=None):
        """
        Returns the authorities' attributes dictionary for each subgraph.
        :return: A dictionary where keys are 'all', 'mt', 're', 'rt' and values are the attributes of the authorities
            of the 'all' graph in other graphs.
        """
        graphs, vert_ids = self.get_authorities()
        auth_vertices_attributes = {}
        for k, g in graphs.items():
            auth_vertices_attributes[k] = [v.attributes() for v in g.vs.select(vert_ids)]


        sg = self.graph_wrapper.graph.subgraph(vert_ids)
        labels = sg.vs['name']
        if svg_filename is not None:
            sg.write_svg(svg_filename, labels=labels, layout='circle', width=800, height=800, vertex_size=30,
                     colors=['#2787C5'] * len(sg.vs))

        return auth_vertices_attributes

    def community_detection_infomap(self):
        """
        Calculates the communities in the current network and saves them as a vertex attribute.
        :return: A community graph where each vertex represents a community. Edges will be weighted based on
        the number of edges connecting each community.
        """
        g = GraphWrapper(self.graph_wrapper)    # è richiesto altrimenti tutte le metriche vengono calcolate diversamente
        g.graph.es['weight'] = 1
        g.graph.simplify(combine_edges=dict(weight='sum'))
        g.compute_community_detection()
        self.graph_wrapper.vs['community'] = g.graph.vs['community']

    # TODO: Non funziona, va in crash
    def create_contract_graph(self, svg_filename=None, vertex_ids_filter=None):
        """
        Contract the vertices of the graph based on communities
        """
        g = GraphWrapper(self.graph_wrapper)
        if vertex_ids_filter is not None:
            g.graph = g.graph.subgraph(vertex_ids_filter)
        g.graph.contract_vertices(mapping=g.graph.vs['community'], combine_attrs='First')
        g.graph.simplify(combine_edges=dict(weight='sum'))
        labels = g.vs['community']
        colors = get_colors(*labels)
        if svg_filename is not None:
            g.graph.write_svg(svg_filename, labels=labels, layout='circle', width=800, height=800, vertex_size=30,
                     colors=colors)
        return g

    def get_border_vertices(self, communities, mode):
        """
        Calculates the border vertices for each community pair.
        :return: The border nodes for each community as a dictionary with the community integer as key and values
        the bridge edge.
        """
        g = self.graph_wrapper
        g.compute_edge_betweenness()
        border_nodes = {}
        for community in communities:
            comm_edges = {}
            for v in g.vs.select(community=community):
                if mode == 'out':
                    incidents = g.graph.incident(v, mode)
                    incidents = [e for e in g.es[incidents] if g.graph.vs[e.target]['community'] in communities and
                                 g.graph.vs[e.target]['community'] != community]
                    for e in incidents:
                        target_community = g.graph.vs[e.target]['community']
                        if e.target == e.source:
                            continue
                        # print(e['edge_betweenness'])
                        if comm_edges.get(target_community) is None:
                            comm_edges[target_community] = e
                        elif comm_edges[target_community]['edge_betweenness'] < e['edge_betweenness']:
                            comm_edges[target_community] = e
                elif mode == 'in':
                    incidents = g.graph.incident(v, mode)
                    incidents = [e for e in g.es[incidents] if g.graph.vs[e.source]['community'] in communities and
                                 g.graph.vs[e.source]['community'] != community]
                    for e in incidents:
                        target_community = g.graph.vs[e.source]['community']
                        if e.target == e.source:
                            continue
                        # print(e['edge_betweenness'])
                        if comm_edges.get(target_community) is None:
                            comm_edges[target_community] = e
                        elif comm_edges[target_community]['edge_betweenness'] < e['edge_betweenness']:
                            comm_edges[target_community] = e

            border_nodes[community] = comm_edges
        return border_nodes

    def calculate_diff_table_graph(self):
        """
        Calculate table differences between the graph and its subnets divided by type
        """
        self.graph_wrapper.divide_combined_net()
        rt = GraphWrapper(self.graph_wrapper.rt_graph)
        re = GraphWrapper(self.graph_wrapper.re_graph)
        mt = GraphWrapper(self.graph_wrapper.mt_graph)
        return an.graph_diff(self.graph_wrapper, rt, mt, re)


def get_colors(*ints):
    colors = []
    for i in ints:
        colors.append('{0:06X}'.format((i % 255)))
    return colors