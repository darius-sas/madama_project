import copy
from .Analysis import timestamp_to_date
import sys
import time
import datetime
from collections import defaultdict, deque

import igraph as ig


class GraphWrapper:
    def __init__(self, graph=None):
        if isinstance(graph, GraphWrapper):
            new_graph = copy.deepcopy(graph)
            self.graph = new_graph.graph
            self.rt_graph = new_graph.rt_graph
            self.re_graph = new_graph.re_graph
            self.mt_graph = new_graph.mt_graph
            self.vs = new_graph.vs
            self.es = new_graph.es
            self.__followers = new_graph.__followers
            self.__calculated_metrics = new_graph.__calculated_metrics
        else:
            self.graph = graph
            if graph is not None:
                self.vs = graph.vs
                self.es = graph.es
            self.rt_graph = None
            self.re_graph = None
            self.mt_graph = None
            self.__followers = None     # Followers node ids and self.graph ids are different!! Use name attribute!
            self.__calculated_metrics = defaultdict(self.dict_default_false_init)

    def load_gml(self, filename):
        """
        Load in memory the graph from a GML file
        :param filename: path of the file
        :return: an igraph object
        """
        g = ig.Graph.Read_GML(filename)
        g.es['timestamp'] = [int(e) for e in g.es['timestamp']]
        self.__init__(g)

    def save_to_file(self, filename, format='gml'):
        """
        Save graph as a file; default is a GML file
        :param filename: path of the file
        :param format: format of the file
        """
        self.graph.save(filename, format=format)

    def reset_calculated_metrics_flag(self):
        """
        Resets the currently calculated metrics. Call this method if you need to recalculate all the metrics.
        """
        self.__calculated_metrics = defaultdict(self.dict_default_false_init)

    def subgraph(self, date_start, date_end, type='all'):
        """
        Create a subgraph of self.graph filtered by date and type of the edges.
        :param date_start: The starting timestamp.
        :param date_end: The ending timestamp.
        :param type: an optional tweet type. Choose from ['RT', 'MT', 'RE'].
        :returns A GraphWrapper object containing the subgraph.
        """
        g = None
        if type == 'all':
            g = GraphWrapper(self.graph.subgraph_edges(self.graph.es.select(timestamp_gt=date_start, timestamp_lt=date_end)))
        else:
            g = GraphWrapper(self.graph.subgraph_edges(self.graph.es.select(timestamp_gt=date_start, timestamp_lt=date_end, type=type)))
        g.reset_calculated_metrics_flag()
        return g

    def subgraph_by_communities(self, communities):
        """
        Create a subgraph of self.graph filtered by communities.
        :param communities: a list containing the communities we want to consider.
        :returns A GraphWrapper object containing the subgraph.
        """
        g = GraphWrapper(self.graph.subgraph(self.graph.vs.select(community_in=communities)))
        return g

    def divide_combined_net(self):
        """
        Partition the combined graph into mentions, retweets and replies
        :returns A triple with the mentions, retweets and replies graphs
        """
        if self.__calculated_metrics['divide_combined_net']:
            return self.mt_graph, self.rt_graph, self.re_graph

        mt_e = self.graph.es.select(type='MT')
        self.mt_graph = self.graph.subgraph_edges(mt_e)

        rt_e = self.graph.es.select(type='RT')
        self.rt_graph = self.graph.subgraph_edges(rt_e)

        re_e = self.graph.es.select(type='RE')
        self.re_graph = self.graph.subgraph_edges(re_e)

        self.__calculated_metrics['divide_combined_net'] = True

        return self.mt_graph, self.rt_graph, self.re_graph

    def compute_vertices_timestamp(self):
        """
        Assign to every vertex the minimum timestamp between all the incidents edges.
        """
        if self.__calculated_metrics['vertices_timestamp']:
            return

        for v in self.graph.vs:
            tmp = [self.graph.es[e]['timestamp'] for e in self.graph.incident(v, mode='ALL')]
            v['timestamp'] = min(tmp)

        self.__calculated_metrics['vertices_timestamp'] = True

    def get_degree_frequency(self, mode='all'):
        """
        Return degree frequency as a dictionary where the keys are the degree and the values are the frequencies.
        :param mode: specify the kind of degree to calculate
        :return A dictionary containing the degree distribution.
        """
        distribution = defaultdict(self.dict_default_zero_init)
        for node in self.graph.vs:
            distribution[node.degree(mode=mode)] += 1
        return distribution

    def get_average_degree(self):
        """
        Calculates the average degree of the graph.
        :return: The average degree of the graph.
        """
        acc = 0.0
        for v in self.graph.vs:
            acc += v.degree()
        return acc / len(self.graph.vs)

    def compute_centrality_measures(self):
        """
        Calculates and saves as vertex attribute the following metrics: degree (+ in and out), betweenness and closeness
        """
        if self.__calculated_metrics['centrality_measures']:
            return
        # degree
        self.graph.vs['degree'] = self.graph.degree()
        # in_degree
        self.graph.vs['in_degree'] = self.graph.degree(mode='IN')
        # out_degree
        self.graph.vs['out_degree'] = self.graph.degree(mode='OUT')
        # betweenness
        self.graph.vs['betweenness'] = self.graph.betweenness(directed=True)
        # closeness
        self.graph.vs['closeness'] = self.graph.closeness()
        # in-out degree ratio
        for vertex in self.graph.vs:
            if vertex['out_degree'] is not 0:
                vertex['in/out'] = vertex['in_degree']/vertex['out_degree']
            else:
                vertex['in/out'] = vertex['in_degree']/sys.float_info.epsilon

        self.__calculated_metrics['centrality_measures'] = True

        self.__calculated_metrics['centrality_measures'] = True

    def compute_edge_date(self):
        """
        Convert the timestamps of the edges to a date format and save them in a 'date' attribute.
        """
        if self.__calculated_metrics['edge_date']:
            return
        for edge in self.graph.es:
            edge['date'] = timestamp_to_date(edge['timestamp'])

        self.__calculated_metrics['edge_date'] = True

    def compute_pagerank(self):
        """
        Compute pagerank on the graph and saves the results as a vertex attribute.
        """
        if self.__calculated_metrics['pagerank']:
            return
        self.graph.vs['pagerank'] = self.graph.pagerank()
        self.__calculated_metrics['pagerank'] = True

    def compute_authority_score(self):
        """
        Compute the Kleinberg's authority score for the vertices of the graph.
        """
        if self.__calculated_metrics['kl_auth']:
            return
        self.graph.vs['kl_hub'] = self.graph.hub_score()
        self.graph.vs['kl_auth'] = self.graph.authority_score()
        self.__calculated_metrics['kl_auth'] = True

    def compute_vertex_connectivity(self):
        """
        Compute the connectivity and vertex connectivity of vertices in the graph.
        """
        if self.__calculated_metrics['connectivity']:
            return
        for v in self.graph.vs:
            v['connectivity'] = len(set(v.neighbors()))
        for v in self.graph.vs:
            neigh = set(v.neighbors())
            v['avg_connectivity'] = 0
            for n in neigh:
                v['avg_connectivity'] += n['connectivity']
            v['avg_connectivity'] /= len(neigh)
        self.__calculated_metrics['connectivity'] = True

    def compute_edge_betweenness(self):
        """
        Computes the edge betweenness and saves it as an edge attribute.
        """
        if self.__calculated_metrics['edge_betweenness']:
            return
        self.graph.es['edge_betweenness'] = self.graph.edge_betweenness()
        self.__calculated_metrics['edge_betweenness'] = True

    def compute_assortativity_degree(self):
        """
        Compute the assortativity of the graph based on the degree
        """
        if self.__calculated_metrics['ass_degree']:
            return
        self.graph['ass_degree'] = self.graph.assortativity_degree()
        self.__calculated_metrics['ass_degree'] = True

    def get_followers(self, vertex):
        """
        Returns all the vertices objects following the given vertex.
        :param vertex: The vertex object.
        :return: A VertexSeq of the vertices following the given vertex id.
        """
        f_vertex = self.__get_corresponding_followers_vertex(vertex)
        return f_vertex.neighbors(mode='IN')

    def get_followed(self, vertex):
        """
        Returns all the vertices followed by the given vertex id.
        :param vertex: The vertex id.
        :return: A VertexSeq of the vertices followed by the given vertex id.
        """
        f_vertex = self.__get_corresponding_followers_vertex(vertex)
        return f_vertex.neighbors(mode='OUT')

    def __get_corresponding_followers_vertex(self, vertex):
        """
        Returns the corresponding follower vertex.
        :param vertex: The vertex object
        :return: The corresponding vertex from __followers
        """
        if self.__followers is None:
            raise ValueError("You must first call compute_followers.")
        return self.__followers.vs.find(name=vertex['name'])

    def get_interval_edge_number(self, timestamp_start=0000000000, timestamp_end=9999999999, measure='s', convert=True):
        """
        Returns a dict containing the number of edges at each moment within a given interval.
        :param timestamp_start: the beginning of the interval to consider
        :param timestamp_end: the end of the interval to consider
        :param measure: the width of the interval to consider; 's'=second, 'm'=minute, 'h'=hour, 'd'=day
        :param convert: enable key conversion to timestamp
        :return: A dictionary having: key = timestamp/date, value = number of edges for that time interval
        """

        if measure == 's':
            measure = '%d/%m/%Y %H:%M:%S'
        elif measure == 'm':
            measure = '%d/%m/%Y %H:%M'
        elif measure == 'h':
            measure = '%d/%m/%Y %H'
        else:
            measure = '%d/%m/%Y'

        conv = lambda ts, measure: timestamp_to_date(ts, measure)

        timestamp_start = conv(timestamp_start, measure)
        timestamp_end = conv(timestamp_end, measure)

        edge_count = {}
        for edge in self.es:
            if timestamp_start <= conv(edge['timestamp'],measure) <= timestamp_end:
                if conv(edge['timestamp'], measure) in edge_count:
                    edge_count[conv(edge['timestamp'], measure)] += 1
                else:
                    edge_count[conv(edge['timestamp'], measure)] = 1

        if convert:
            converted_edge_count = {}
            for key in edge_count:
                new_key = time.mktime(datetime.datetime.strptime(str(key), measure).timetuple())
                converted_edge_count[round(new_key)] = edge_count[key]
            edge_count = converted_edge_count
        return edge_count

    def load_followers(self, followersfile):
        """
        Loads the followers graph and calculates followers and followed for each node.
        If no followers are found in the __follower graph, 0 is assigned.
        :param followersfile: The path to the followers file.
        """
        self.__followers = ig.Graph.Read_GML(followersfile)
        for v in self.graph.vs:
            try:
                f_vertex = self.__followers.vs.find(name=v['name'])
            except:
                f_vertex = None
            if f_vertex is not None:
                v['followers'] = f_vertex.indegree()
                v['followed'] = f_vertex.outdegree()
            else:
                v['followers'] = 0
                v['followed'] = 0

    def get_global_clustering_coefficient(self):
        """
        Returns the global clustering coefficient for the current graph.
        :return: A real number representing the global clustering coefficient.
        """
        return self.graph.transitivity_undirected()

    def get_avglocal_clustering_coefficient(self):
        """
        Calculates the average transitivity locally for each node, then returns the average.
        :return:
        """
        return self.graph.transitivity_avglocal_undirected("zero")

    def compute_local_clustering_coefficient(self):
        """
        Calculates the local clustering coefficient for every node
        and saves the values as an attribute named 'cluster_coeff'.
        ATTENTION: This is the only way to achieve the same results as cytoscape!
                   Calling transitivity_local_undirected differently will produce different values!
        """
        if self.__calculated_metrics['cluster_coeff']:
            return
        for v in self.graph.vs:
            v['cluster_coeff'] = self.graph.transitivity_local_undirected(v, mode="zero")
        self.__calculated_metrics['cluster_coeff'] = True

    def get_jaccard_matrix(self):
        """
        Return the jaccard similarity matrix of the graph
        :return: Jaccard similarity matrix
        """
        return self.graph.similarity_jaccard(loops=False)

    def compute_community_detection(self):
        """
        Infomap Community detection. Calculate communities and assign, for each vertex, the corresponding community
        """
        if self.__calculated_metrics['community']:
            return
        clusters = self.graph.community_infomap()
        membership = clusters.membership
        self.graph.vs['community'] = membership
        self.__calculated_metrics['community'] = True

    def group_edges_by_type(self):
        """
        Removes duplicated edges from the graph and adds an edge of each type containing a weight attribute.
        The weight is a counter of the number of edges of that type that were removed.
        A timestamps attribute will be added containing all the timestamps of the old edges.
        """
        self.graph.es['grouped'] = False
        for v1 in self.graph.vs:
            if v1.degree() is 0:
                continue
            for v2 in v1.neighbors():
                edges = self.graph.es.select(_source=v1.index, _target=v2.index, grouped=False)
                for e_type in set(edges['type']):
                    t_edges = edges.select(type=e_type)
                    self.graph.delete_edges(t_edges)
                    self.graph.add_edge(v1, v2, type=e_type, weight=len(t_edges), grouped=True)
        self.reset_calculated_metrics_flag()

    def compute_discussion_graph(self, start_node, start_edge, interval=7200, cutoff=0.1):
        """
        Calculates the discussion graph based on the following assumption:
            nodes will interact with the source node within a given amount of time from starting edge (tweet)
            and his children will interact with him within a shorter amount of time
        The interval decreases at each depth increase with the following formula:
            interval = interval * ((1 - cutoff)**depth)
        Default interval is set to 7200, while cutoff is set to 0.1.
        At the end of execution the graph will be colored as following:
            RED   ->  the edges and nodes that are considered to be part of the same discussion of the start_edge
            WHITE ->  the edges and nodes that are NOT considered to be part of the discussion
        :param start_node: The starting node.
        :param start_edge: An edge connected with the given start_node that represents the discussion starting tweet.
        :param interval: The starting interval value, default is 7200
        :param cutoff: The decrease rate of the interval at each depth. Default is 0.1
        """
        visited = set(list())
        queue = deque(list())
        depth = 0

        self.vs['color'] = 'white'
        self.es['color'] = 'white'
        self.vs['previous_ts'] = 0

        start_edge['color'] = 'red'
        queue.append(start_node)
        queue.append(None)

        while len(queue) > 0:
            current = queue.popleft()
            if current is None:
                depth += 1
                queue.append(None)
                if queue[0] is None:
                    break
                else:
                    continue
            visited.add(current)
            current['color'] = 'red'    # I nodi rossi sono i nodi raggiunti da almeno un arco rosso
            incidents = self.graph.es[self.graph.incident(current, mode='ALL')]
            current['previous_ts'] = max([e['timestamp'] for e in incidents.select(color='red')])

            white_incidents = incidents.select(color='white')
            for e in white_incidents:
                cutoff_interval = interval * ((1 - cutoff) ** depth)
                if current['previous_ts'] < e['timestamp'] <= current['previous_ts'] + cutoff_interval:
                    e['color'] = 'red'  # Colora gli archi che soddisfano la condizione
                    if self.graph.vs[e.source] == current:
                        target_node = self.graph.vs[e.target]
                    else:
                        target_node = self.graph.vs[e.source]
                    if target_node not in visited:
                        queue.append(target_node)


    def dict_default_false_init(self):
        return False

    def dict_default_zero_init(self):
        return 0
