from collections import defaultdict
import datetime
import pytz

def vertex_diff(vertices):
    """
    Return a table that represent the main statistics for every vertex given in input
    :param vertices: an igraph vertex sequence.
    :return: a list of list, representing a table where rows represent a vertex, and columns the vertex's attributes.
    """
    table = []
    col_names = ['name', 'degree', 'in_degree', 'out_degree', 'betweenness', 'closeness', 'pagerank',
                 'kl_auth', 'kl_hub', 'connectivity', 'avg_connectivity', 'in/out', 'followers', 'followed']
    attr_names = vertices[0].attribute_names()
    table.append(col_names)
    for v in vertices:
        vertex_attr = []
        for attr in col_names:
            if attr not in attr_names:
                break
            vertex_attr.append(v[attr])
        table.append(vertex_attr)
    return table

def print_table(table):
    """
    Print the table given in input.
    :param table: A table to print out.
    """
    col_width = [0]*len(table[0])
    i = 0
    j = 0
    while j < len(table[1]):
        while i < len(table):
            if len(str(table[i][j])) > col_width[j]:
                col_width[j] = len(str(table[i][j]))
            i += 1
        i = 0
        j += 1

    for row in table:
        print("| ", " | ".join(str("{:<" + str(col_width[i]) + "}").format(str(x)) for i, x in enumerate(row)), " |")

def graph_diff(*graphs):
    """
    Prints the differences between the given graphs objects.
    :param graphs: A list of GraphWrapper objects
    :return: A table where each row represents a graph, and columns represents graph's attributes.
    """
    table = []
    col_names = ['assortativity', 'n_communities', 'n_nodes', 'n_edges', 'clustering_coeff',
                 'max_timestamp', 'min_timestamp']
    table.append(col_names)
    for gw in graphs:
        gw.compute_assortativity_degree()
        graph_attributes = []
        graph_attributes.extend(gw.graph.attributes())
        graph_attributes.extend(gw.graph.vs.attributes())

        graph_attr = []
        if 'ass_degree' in graph_attributes:
            graph_attr.append(gw.graph['ass_degree'])
        else:
            graph_attr.append('X')
        if 'community' in graph_attributes:
            graph_attr.append(len(attribute_counter(gw.graph.vs, 'community')))
        else:
            graph_attr.append('X')
        graph_attr.append(len(gw.graph.vs))
        graph_attr.append(len(gw.graph.es))
        graph_attr.append(gw.get_global_clustering_coefficient())
        timestamps = gw.graph.es['timestamp']
        max_timestamp = max(timestamps)
        min_timestamp = min(timestamps)
        graph_attr.append(timestamp_to_date(max_timestamp))
        graph_attr.append(timestamp_to_date(min_timestamp))
        table.append(graph_attr)
    return table


def attribute_counter(sequence, attribute):
    list_counter = defaultdict(lambda: 0)
    for a in sequence[attribute]:
        list_counter[a] += 1
    return list_counter

def timestamp_to_date(ts, format='%d/%m/%Y %H:%M:%S'):
    """
    Converts the given timestamp to a string with the given format.
    :param ts: A timestamp integer
    :param format: The format of the date string.
    :return: A string representing the date.
    """
    timezone = pytz.timezone('Europe/Rome')
    return datetime.datetime.fromtimestamp(ts, tz=timezone).strftime(format)

def print_to_csv(filename, table, sep=';'):
    with open(filename, mode='w') as f:
        for row in table:
            f.write(sep.join([str(x) for x in row]))
            f.write('\n')
