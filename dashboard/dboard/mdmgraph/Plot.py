from plotly.graph_objs import *
import plotly.offline as po


def plot_line_serie(x_serie, y_serie, label="", type="lines", filename='lineplot'):
    """
    Plots a single lines series.
    :param x_serie: The x values of the series.
    :param y_serie: The y values of the series.
    :param label: The data labels for the x series.
    :param type: The type of the plot, default is 'lines'.
    :param filename: The filename where to save the data.
    """
    plot_lines_series([[x_serie, y_serie, type, label]], filename)


def plot_lines_series(series, zoom_range_x=None, zoom_range_y=None):
    """
    Plots the given data and saves it with the given name.
    :param series: A list where each element follows the following convention:
        [x, y, plot_type, series_name]
    :param filename: The filename of the current series plot.
    """
    data = []
    for i in range(0, len(series)):
        trace = Scatter(
            x=series[i][0],
            y=series[i][1],
            mode=series[i][2],
            name=series[i][3]
        )
        data.append(trace)
    layout = Layout(
                    xaxis=dict(range=zoom_range_x),
                    yaxis=dict(range=zoom_range_y),
                    )
    fig = Figure(data=data, layout=layout)
    return po.plot(fig, output_type='div', auto_open=False, include_plotlyjs=False)


def plot_heatmap(colorscale, x, labels, *y):
    assert len(labels) == len(y)

    data = [
        Heatmap(
            z=y,
            x=x,
            y=labels,
            type='heatmap',
            colorscale=colorscale,  # Blackbody, Electric, Earth, Bluered, YIOrRd, RdBu, Portland, Picnic, Jet
        )
    ]

    layout = Layout(
        xaxis=dict(ticks='', nticks=10),
        yaxis=dict(ticks='')
    )

    fig = Figure(data=data, layout=layout)
    return po.plot(fig, output_type='div', auto_open=False, include_plotlyjs=False)


def plot_histogram(plot_title, zoom_rangeX, zoom_rangeY, *series):
    data = []
    for i in series:
        trace = Histogram(
            x=i[0],
            opacity=0.75,
            name=i[1]
        )
        data.append(trace)

    layout = Layout(barmode='overlay', title=plot_title, xaxis=dict(range=zoom_rangeX), yaxis=dict(range=zoom_rangeY))
    fig = Figure(data=data, layout=layout)

    return po.plot(fig, output_type='div', auto_open=False, include_plotlyjs=False)


def plot_network_graph(graph):
    N = len(graph.graph.vs)
    L = len(graph.graph.es)

    G = graph.graph

    labels=[]
    group=[]

    for node in graph.graph.vs:
        labels.append(str(int(node['community'])))
        group.append(node['community'])

    layt = G.layout('kk', dim=3)
    Xn = [layt[k][0] for k in range(N)]# x-coordinates of nodes
    Yn = [layt[k][1] for k in range(N)]# y-coordinates
    Zn = [layt[k][2] for k in range(N)]# z-coordinates
    Xe = []
    Ye = []
    Ze = []
    for e in graph.graph.es:
        Xe+=[layt[e.source][0],layt[e.target][0], None]# x-coordinates of edge ends
        Ye+=[layt[e.source][1],layt[e.target][1], None]
        Ze+=[layt[e.source][2],layt[e.target][2], None]
    trace1=Scatter3d(x = Xe,
                   y=Ye,
                   z=Ze,
                   mode='line',
                   line=Line(color='rgb(125,125,125)', width=1),
                   hoverinfo='none'
                   )
    trace2=Scatter3d(x=Xn,
                   y=Yn,
                   z=Zn,
                   mode='marker',
                   name='actors',
                   marker=Marker(symbol='circle',
                                 size=15,
                                 color=group,
                                 colorscale='Earth',
                                 line=dict(width=0.5)
                                 ),
                   text=labels,
                   hoverinfo='text'
                   )
    axis=dict(showbackground=False,
              showline=False,
              zeroline=False,
              showgrid=False,
              showticklabels=False,
              title=''
              )
    layout = Layout(
             title="Interazione tra le principali communities",
             width=1000,
             height=1000,
             showlegend=False,
             scene=Scene(
             xaxis=XAxis(axis),
             yaxis=YAxis(axis),
             zaxis=ZAxis(axis),
            ),
         margin=Margin(
            t=100
        ),
        hovermode='closest',
        annotations=Annotations([
               Annotation(
               showarrow=False,
                text="Interazione tra le principali communities",
                xref='paper',
                yref='paper',
                x=0,
                y=0.1,
                xanchor='left',
                yanchor='bottom',
                font=Font(
                size=14
                )
                )
            ]),    )
    data=Data([trace1, trace2])
    fig=Figure(data=data, layout=layout)

    return po.plot(fig, output_type='div', auto_open=False, include_plotlyjs=False)

