Creator "igraph version 0.7.1 Wed Jun 28 17:20:52 2017"
Version 1
graph
[
  directed 1
  node
  [
    id 18
    name "677"
    followers 3398
    followed 33
    degree 3915
    indegree 3883
    outdegree 32
    betweenness 5477101.53156431
    closeness 0.388841773685486
    inout 121.34375
  ]
  node
  [
    id 20
    name "88"
    followers 3247
    followed 60
    degree 7213
    indegree 7194
    outdegree 19
    betweenness 6082127.42940217
    closeness 0.429479459387127
    inout 378.631578947368
  ]
  node
  [
    id 21
    name "220"
    followers 2399
    followed 11
    degree 1106
    indegree 1106
    outdegree 0
    betweenness 0
    closeness 0.302811585998679
    inout 4.98098118787177e+18
  ]
  node
  [
    id 48
    name "13813"
    followers 378
    followed 17
    degree 538
    indegree 518
    outdegree 20
    betweenness 1188053.83832424
    closeness 0.355072463768116
    inout 25.9
  ]
  node
  [
    id 58
    name "13808"
    followers 390
    followed 115
    degree 537
    indegree 488
    outdegree 49
    betweenness 2527951.78178938
    closeness 0.346645065478601
    inout 9.95918367346939
  ]
  node
  [
    id 243
    name "1988"
    followers 1860
    followed 192
    degree 1161
    indegree 1157
    outdegree 4
    betweenness 641595.276003118
    closeness 0.332384009942005
    inout 289.25
  ]
  node
  [
    id 391
    name "3998"
    followers 432
    followed 2
    degree 1229
    indegree 1090
    outdegree 139
    betweenness 920484.502614389
    closeness 0.363723934723481
    inout 7.84172661870504
  ]
  edge
  [
    source 18
    target 18
    timestamp 1341415503
    type "MT"
  ]
  edge
  [
    source 18
    target 18
    timestamp 1341409331
    type "MT"
  ]
  edge
  [
    source 18
    target 20
    timestamp 1341385483
    type "RT"
  ]
  edge
  [
    source 18
    target 20
    timestamp 1341385471
    type "RT"
  ]
  edge
  [
    source 18
    target 20
    timestamp 1341378653
    type "RT"
  ]
  edge
  [
    source 18
    target 48
    timestamp 1341385471
    type "MT"
  ]
  edge
  [
    source 18
    target 391
    timestamp 1341385471
    type "MT"
  ]
  edge
  [
    source 20
    target 20
    timestamp 1341582740
    type "MT"
  ]
  edge
  [
    source 20
    target 20
    timestamp 1341577465
    type "MT"
  ]
  edge
  [
    source 20
    target 20
    timestamp 1341386203
    type "MT"
  ]
  edge
  [
    source 20
    target 20
    timestamp 1341380594
    type "MT"
  ]
  edge
  [
    source 20
    target 20
    timestamp 1341374938
    type "MT"
  ]
  edge
  [
    source 20
    target 48
    timestamp 1341384767
    type "MT"
  ]
  edge
  [
    source 20
    target 48
    timestamp 1341383428
    type "MT"
  ]
  edge
  [
    source 20
    target 48
    timestamp 1341381270
    type "MT"
  ]
  edge
  [
    source 20
    target 391
    timestamp 1341384767
    type "MT"
  ]
  edge
  [
    source 20
    target 391
    timestamp 1341381062
    type "MT"
  ]
  edge
  [
    source 20
    target 391
    timestamp 1341380594
    type "MT"
  ]
  edge
  [
    source 20
    target 391
    timestamp 1341379236
    type "MT"
  ]
  edge
  [
    source 20
    target 391
    timestamp 1341378303
    type "MT"
  ]
  edge
  [
    source 48
    target 48
    timestamp 1341382676
    type "MT"
  ]
  edge
  [
    source 58
    target 20
    timestamp 1341378923
    type "RT"
  ]
  edge
  [
    source 58
    target 20
    timestamp 1341370873
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341589102
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341489793
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341476677
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341428214
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341423045
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341399041
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341389293
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341370873
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341317630
    type "MT"
  ]
  edge
  [
    source 58
    target 58
    timestamp 1341296608
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341374445
    type "RE"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341565774
    type "RT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341485303
    type "RT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341485955
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341401531
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341385009
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384896
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384723
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384632
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384526
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384505
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384338
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384290
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341384245
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383953
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383748
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383694
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383505
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383406
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383312
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383132
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341383046
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382842
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382738
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382577
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382305
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382180
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341382037
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381816
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381562
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381479
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381388
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381283
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381204
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381167
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341381079
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380987
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380908
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380815
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380745
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380654
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380567
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380478
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380352
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380248
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380181
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380079
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341380040
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379698
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379558
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379398
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379258
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379110
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341379026
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378858
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378813
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378630
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378561
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378446
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378370
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378311
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378274
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378254
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378235
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378191
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378169
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378128
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378070
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341378019
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377970
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377941
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377814
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377722
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377599
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341377147
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341376871
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341376839
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341376692
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341376534
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341376080
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341375754
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341375526
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341375238
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341374904
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341374445
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341372441
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341353436
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341352785
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341352037
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341348336
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341341001
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341339428
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341310888
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341243335
    type "MT"
  ]
  edge
  [
    source 391
    target 20
    timestamp 1341234883
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341383748
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341383694
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341383406
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341382842
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341382577
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341382037
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341381204
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341380567
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341374445
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341341001
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341316544
    type "MT"
  ]
  edge
  [
    source 391
    target 48
    timestamp 1341310888
    type "MT"
  ]
  edge
  [
    source 391
    target 58
    timestamp 1341378858
    type "RE"
  ]
  edge
  [
    source 391
    target 58
    timestamp 1341378858
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341382343
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341381204
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341381167
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341381165
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341381079
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341380567
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341379816
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341379712
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341379390
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341378937
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341378813
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341378738
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341378566
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341378274
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341377752
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341377259
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341376839
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341376217
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341376080
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341375238
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341374904
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341352037
    type "MT"
  ]
  edge
  [
    source 391
    target 391
    timestamp 1341310888
    type "MT"
  ]
]
