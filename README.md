##**MaDaMa Project**
Questo progetto ha come obbiettivo l'analisi di una rete Twitter scaturita dall'annuncio della scoperta del bosone di Higgs, avvenuto il 4 Luglio 2012.

##Struttura progetto
* _./dashboard_: questa cartella contiene la dashboard del progetto, dove si può analizzare una delle reti, presenti nella cartella _dataset_ in formato _.gml_. Per eseguire l'analisi senza errori la rete deve contenere i tre tipi di archi (MT, RE, RT) e il campo _timestamp_. 
Per la rete caricata vengono visualizzate tutte le misure, i grafici e le sottoreti prodotte durante l'analisi.
La dashboard è sviluppata usando _Django_.
    * _esecuzione_: eseguire 
        `python manage.py runserver` in seguito andare all'indirizzo specificato in console e aggiungere _/dash_ all'indirizzo, ad esempio `http://127.0.0.1:8000/dash`
    * _/mdmgraph_: in questa cartella sono contenuti gli script in python che vengono eseguiti in backend per produrre i risultati mostrati nella dashboard.
    
* _./dataset_: in questa cartella sono presenti tutti i grafi prodotti durante l'analisi, in particolare abbiamo:
    * _combined-timestamp.gml_ rappresenta il grafo completo con i timestamp sugli archi;
    *  _mt_combined_node_timed.gml_ rappresenta il sotto grafo composto soltanto da archi di tipo Mention e con il timestamp. Questo grafo non può essere caricato nella dashboard perché l'analisi viene fatta solo sui grafi completi.
    * _rt_combined_node_timed.gml_ rappresenta la sotto rete dei Retweet e valgono le stesse considerazioni fatte per il grafo precedente.
    * _re_combined_node_timed.gml_ rappresenta il sottografo delle Reply.
    * _followers.gml_ rappresenta le relazioni follower, followee tra i nodi della rete completa.
    
* _./scripts_python_: in questa cartella sono presenti tutti gli scripts sviluppati e necessari all'analisi. Questa cartella è in realtà una copia di _mdmgraph_ presente in dashboard. 
    * _GraphWrapper.py_ contiene la classe che gestisce il grafo ed esegue le principali operazioni.
    * _ResearchQuestion.py_ per ogni domanda che ci siamo posti contiene una serie di metodi usati per arrivare alla rispota.
    * _Analysis.py_ contiene metodi per il raffronto tra grafi e nodi.
    * _Plot.py_ contiene metodi per il plot dei risultati ottenuti.
        

##Installazione
Tutti i pacchetti necessari per eseguire il progetto sono contenuti in _requirements.txt_ per installarli tramite pip nel proprio ambiente di sviluppo digitare semplicemente il seguente comando:
`pip install -r requirements.txt`
Per eseguire correttamente il codice è necessaria la versione Python 3.5.

 