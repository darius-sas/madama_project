from scripts_python.ResearchQuestions import *
from scripts_python.GraphWrapper import *
from scripts_python.Analysis import *
from scripts_python.Plot import *
import string
import random
import igraph as ig
from multiprocessing import Process, freeze_support

file_path = '../dataset/combined-timestamp.gml'
# CREAZIONE GRAFI CON MISURE CALCOLATE DA PYTHON
# g = GraphWrapper()
# g.load_gml('../dataset/grafo_con_communities_infomap_aggregated.gml')
# subg = g.subgraph_by_communities([0, 1, 2, 4, 8, 9])
# g.compute_centrality_measures()
# g.compute_pagerank()
# g.compute_authority_score()
# g.compute_vertex_connectivity()
# g.compute_local_clustering_coefficient()
# g.load_followers('../dataset/followers.gml')
# g.save_to_file('../relazione/dati/network_with_measures.gml')

# ISTOGRAMMI DEI GRADI PER LE RETI
# filenames = ['../dataset/rt_combined.gml','../dataset/mt_combined.gml','../dataset/re_combined.gml']
# i = 0
# names = ['Retweet', 'Mention', 'Reply']
# for file in filenames:
#     g = GraphWrapper()
#     g.load_gml(file)
#     plt = Plot()
#     plt.plot_histogram(names[i], [0,50], [0, 11000], [g.graph.degree(mode='IN'), 'IN degree'], [g.graph.degree(mode='OUT'), 'OUT degree'])
#     i += 1

# NETWORK GRAPH
# plt = Plot()
# plt.plot_network_graph(subg)
# rq = RSQ0('../dataset/combined-timestamp.gml')
# rq.
#
# exit()

# PONTI TRA COMMUNITY IN E OUT
rq = RSQ3('../dataset/new-infomap-community.gml')
modes = ['in', 'out']
for mode in modes:
    borders = rq.get_border_vertices([0, 1, 2, 3, 7, 8], mode=mode)
    vertices = set()
    for com in borders.keys():
        print('Community ', com, ': ')
        for com_target in borders[com].keys():
            vertices.add(rq.graph_wrapper.graph.vs[borders[com][com_target].source])
            print('interact with: ', com_target,
                  'from ', rq.graph_wrapper.graph.vs[borders[com][com_target].source]['name'],
                  'to ', rq.graph_wrapper.graph.vs[borders[com][com_target].target]['name'],
                  'eb ', borders[com][com_target]['edge_betweenness'])
    rq.compute_metrics()
    a = Analysis()
    a.print_table(a.vertex_diff(list(vertices)))

# m = max(rq.graph_wrapper.es['edge_betweenness'])
# edge = rq.graph_wrapper.graph.es.find(edge_betweenness=m)
# print('S: ', edge.source, 'T: ', edge.target)

# rq.discussion_subgraph_analysis(cutoff=0.5, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.25, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.1, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.05, save_to_file=True)

# r = RSQ1(file_path)
# r.plot_tweets_per_second_heatmap()
# r.plot_tweets_per_sec_percentage(['all', 're', 'rt', 'mt'])
# r.plot_tweets_per_sec_percentage_all(['all', 're', 'rt', 'mt'])
# r.plot_cumulative_tweets()
