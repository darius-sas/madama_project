import numpy
import time
import scipy as sp
import operator
from scripts_python import Plot
from scripts_python.Analysis import *
from scripts_python.GraphWrapper import GraphWrapper


class ResearchQuestions:
    def __init__(self, dataset_name):
        self.graph_wrapper = GraphWrapper()
        self.graph_wrapper.load_gml(dataset_name)

    def compute_metrics(self):
        self.graph_wrapper.compute_centrality_measures()
        self.graph_wrapper.compute_pagerank()
        self.graph_wrapper.compute_authority_score()
        self.graph_wrapper.compute_vertex_connectivity()
        self.graph_wrapper.compute_local_clustering_coefficient()
        self.graph_wrapper.compute_edge_betweenness()
        self.graph_wrapper.load_followers('../dataset/followers.gml')

class RSQ0(ResearchQuestions):
    def __init__(self, path):
        super(RSQ0, self).__init__(path)

    def in_out_hist_chart(self):
        """
        Plot the degree distribution as a line chart for the net and its subnets divided by category
        """
        plt = Plot.Plot()
        self.graph_wrapper.divide_combined_net()
        graphs = [GraphWrapper(self.graph_wrapper.rt_graph), GraphWrapper(self.graph_wrapper.re_graph),
                  GraphWrapper(self.graph_wrapper.mt_graph)]

        lines_names = ['Retweet', 'Reply', 'Mention']
        i = -1
        for g in graphs:
            in_degree_dict = g.get_degree_frequency(mode='in')
            out_degree_dict = g.get_degree_frequency(mode='out')
            degree_in = []
            freq_in = []
            for key in sorted(in_degree_dict):
                degree_in.append(key)
                freq_in.append(in_degree_dict[key])

            degree_out = []
            freq_out = []
            for key in sorted(out_degree_dict):
                degree_out.append(key)
                freq_out.append(out_degree_dict[key])

            series = [[degree_in, freq_in, 'lines', 'IN degree'],
                      [degree_out, freq_out, 'lines', 'OUT degree']]
            i += 1
            plt.plot_lines_series(title=lines_names[i], series=series, zoom_range_x=[0, 50], zoom_range_y=[0, 11000])
            time.sleep(1)

    def in_out_lines_chart(self):
        """
        Plot the degree distribution as a line chart for the net and its subnets divided by category
        """
        plt = Plot.Plot()
        self.graph_wrapper.divide_combined_net()
        graphs = [GraphWrapper(self.graph_wrapper.rt_graph), GraphWrapper(self.graph_wrapper.re_graph),
                  GraphWrapper(self.graph_wrapper.mt_graph)]

        lines_names = ['Retweet', 'Reply', 'Mention']
        i = -1
        for g in graphs:
            in_degree_dict = g.get_degree_frequency(mode='in')
            out_degree_dict = g.get_degree_frequency(mode='out')
            degree_in = []
            freq_in = []
            for key in sorted(in_degree_dict):
                degree_in.append(key)
                freq_in.append(in_degree_dict[key])

            degree_out = []
            freq_out = []
            for key in sorted(out_degree_dict):
                degree_out.append(key)
                freq_out.append(out_degree_dict[key])

            series = [[degree_in, freq_in, 'lines', 'IN degree'],
                      [degree_out, freq_out, 'lines', 'OUT degree']]
            i += 1
            plt.plot_lines_series(title=lines_names[i], series=series, zoom_range_x=[0, 50], zoom_range_y=[0, 11000])
            time.sleep(1)

    def produce_diff_table_graph(self):
        """
        Calculate table differences between the graph and its subnets divided by type
        """
        self.graph_wrapper.divide_combined_net()
        rt = GraphWrapper(self.graph_wrapper.rt_graph)
        re = GraphWrapper(self.graph_wrapper.re_graph)
        mt = GraphWrapper(self.graph_wrapper.mt_graph)
        graph_series = [self.graph_wrapper, rt, re, mt]
        for g in graph_series:
            g.compute_centrality_measures()
            g.compute_vertex_connectivity()
            g.compute_local_clustering_coefficient()
            g.compute_assortativity_degree()

        a = Analysis()
        a.print_table(a.graph_diff(self.graph_wrapper, rt, mt, re))

class RSQ1(ResearchQuestions):
    def __init__(self, path):
        super(RSQ1, self).__init__(path)

    def get_tweets_frequency(self):
        """
        Calculates the tweets frequency at each timestamp.
        :return: A tuple where the first element are the unique timestamps of the graph, the second element is a
        dictionary with the following keys 'all', 'mt', 're', 'rt' and values
        are the number of edges at each unique timestamp.
        """
        g = self.graph_wrapper.graph
        self.graph_wrapper.divide_combined_net()
        re = self.graph_wrapper.re_graph
        rt = self.graph_wrapper.rt_graph
        mt = self.graph_wrapper.mt_graph

        convert = lambda t: datetime.datetime.fromtimestamp(t).strftime('%-d/%m %H:%M')

        all_timestamp = [convert(t) for t in g.es['timestamp']]
        re_timestamp = [convert(t) for t in re.es['timestamp']]
        rt_timestamp = [convert(t) for t in rt.es['timestamp']]
        mt_timestamp = [convert(t) for t in mt.es['timestamp']]

        all_timestamp.sort()
        re_timestamp.sort()
        rt_timestamp.sort()
        mt_timestamp.sort()

        all_dict = {}
        re_dict = {}
        rt_dict = {}
        mt_dict = {}

        for t in all_timestamp:
            all_dict[t] = 0
            re_dict[t] = 0
            mt_dict[t] = 0
            rt_dict[t] = 0

        for t in all_timestamp:
            all_dict[t] += 1

        for t in re_timestamp:
            re_dict[t] += 1

        for t in rt_timestamp:
            rt_dict[t] += 1

        for t in mt_timestamp:
            mt_dict[t] += 1

        frequencies = {'re': list(re_dict.values()), 'rt': list(rt_dict.values()),
                  'mt': list(mt_dict.values()), 'all': list(all_dict.values())}

        unique_timestamps = list(all_dict.keys())

        return unique_timestamps, frequencies

    def get_cumulative_tweets(self):
        """
        Calculates the cumulative tweets per second.
        :return: A tuple where the first element represents the timestamps, and the second is a dictionary
        with the following keys 'all', 'rt', 're', 'mt', values is a the number of tweets at each timestamp, ordered
        to match the timestamps of the first tuple element.
        """
        timestamps, frequencies = self.get_tweets_frequency()
        data = frequencies['all']
        all_tweets = [data[0]]*len(data)

        re = frequencies['re']
        re_interact = [re[0]]*len(re)

        rt = frequencies['rt']
        rt_interact = [rt[0]]*len(rt)

        mt = frequencies['mt']
        mt_interact = [mt[0]]*len(mt)

        for i in range(1, len(data)):
            all_tweets[i] = all_tweets[i-1] + data[i]
        for i in range(1, len(re)):
            re_interact[i] = re_interact[i-1] + re[i]
        for i in range(1, len(rt)):
            rt_interact[i] = rt_interact[i-1] + rt[i]
        for i in range(1, len(mt)):
            mt_interact[i] = mt_interact[i-1] + mt[i]

        return timestamps, {'all': all_tweets, 're': re_interact, 'rt': rt_interact, 'mt': mt_interact}

    def plot_tweets_per_second_heatmap(self):
        """
        Create heatmap tweets per minute
        """
        timestamps, frequencies = self.get_tweets_frequency()
        plt = Plot.Plot()
        plt.plot_heatmap('Grafico attività', 'Earth', timestamps, ['re', 'rt', 'mt', 'all'], *list(frequencies.values()))

    def get_highest_activity_timestamps(self, timestamp_start=0000000000, timestamp_end=9999999999, measure='s', convert=True):
        """
        Returns the timestamps with the highest increase in tweets per second. A timestamp interval can be specified
        with a given precision.
        :param timestamp_start: the starting timestamp.
        :param timestamp_end: the ending timestamp.
        :param measure: the width of the interval to consider; 's'=second, 'm'=minute, 'h'=hour, 'd'=day.
        :param convert: whether to convert or not the timestamp to date format or not.
        :return: a list with the timestamps with the maximum increase in tweets per second.
        """
        g = self.graph_wrapper
        sequence = g.get_interval_edge_number(timestamp_start=timestamp_start, timestamp_end=timestamp_end, measure=measure, convert=convert)

        values = []
        for val in sequence.values():
            values.append(val)

        # calculate derivative vector
        deriv = sp.diff(values)

        # select highest indexes
        highest_index = numpy.where(deriv == max(deriv))

        # create the keys list
        keys_list = list(sequence.keys())

        # select the timestamps that correspond to the high index
        timestamps = []
        for high_index in highest_index[0]:
            timestamps.append(keys_list[high_index])

        return timestamps

    def plot_cumulative_tweets(self):
        """
        Plots the cumulative tweets at each timestamp present in the graph.
        """
        timestamps, frequencies = self.get_cumulative_tweets()
        series = [[timestamps, frequencies['all'], "lines", "all"],
                  [timestamps, frequencies['re'], "lines", "re"],
                  [timestamps, frequencies['rt'], "lines", "rt"],
                  [timestamps, frequencies['mt'], "lines", "mt"]]

        plt = Plot.Plot()
        plt.plot_lines_series(series)

    def plot_tweets_per_sec_percentage(self, tweets_types):
        """
        Plots the number of tweets, of each type, twitted per second relative
        to the total number of tweets (for each type).
        """
        timestamps, frequencies = self.get_tweets_frequency()
        all_d = frequencies['all']
        re_d = frequencies['re']
        rt_d = frequencies['rt']
        mt_d = frequencies['mt']

        for i in range(0, len(all_d)):
            all_d[i] = (all_d[i]/len(self.graph_wrapper.graph.es))*100
            re_d[i] = (re_d[i]/len(self.graph_wrapper.re_graph.es))*100
            rt_d[i] = (rt_d[i]/len(self.graph_wrapper.rt_graph.es))*100
            mt_d[i] = (mt_d[i]/len(self.graph_wrapper.mt_graph.es))*100

        series = []
        for t in tweets_types:
            series.append([timestamps, frequencies[t], "lines", "percentage " + t])

        plt = Plot.Plot()
        plt.plot_lines_series(series)

    def plot_tweets_per_sec_percentage_all(self, tweets_types):
        """
        Plots the number of tweets, of each type, twitted per second relative
        to the total number of tweets.
        """
        timestamps, frequencies = self.get_tweets_frequency()
        all_d = frequencies['all']
        re_d = frequencies['re']
        rt_d = frequencies['rt']
        mt_d = frequencies['mt']

        for i in range(0, len(all_d)):
            all_d[i] = (all_d[i]/len(self.graph_wrapper.graph.es))*100
            re_d[i] = (re_d[i]/len(self.graph_wrapper.graph.es))*100
            rt_d[i] = (rt_d[i]/len(self.graph_wrapper.graph.es))*100
            mt_d[i] = (mt_d[i]/len(self.graph_wrapper.graph.es))*100

        series = []
        for t in tweets_types:
            series.append([timestamps, frequencies[t], "lines", "percentage " + t])

        plt = Plot.Plot()
        plt.plot_lines_series(series)


class RSQ2(ResearchQuestions):
    def __init__(self, graph):
        super(RSQ2, self).__init__(graph)

    def get_autorities_old(self):
        """
        --- DEPRECATED ---
        Calculates the autorities for the current instance graph object and returns a dictionary containing
        4 subgraphs, 1 subraph for each subnetwork and a subgraph for the combined graph.
        :return: A dictionary where the keys are 'all', 'mt', 'rt', 're', and the values are the corresponding
        autorities subgraphs.
        """
        vertices = self.graph_wrapper.graph.vs

        in_degree = {}
        page_rank = {}
        authority_score = {}
        for v in vertices:
            in_degree[v['id']] = v['in_degree']
            page_rank[v['id']] = v['pagerank']
            authority_score[v['id']] = v['kl_auth']

        sorted_in_degree = sorted(in_degree.items(), key=operator.itemgetter(1), reverse=True)
        best_10_indegree = sorted_in_degree[0: 10]

        sorted_page_rank = sorted(page_rank.items(), key=operator.itemgetter(1), reverse=True)
        best_10_page_rank = sorted_page_rank[0: 10]

        sorted_auth_score = sorted(authority_score.items(), key=operator.itemgetter(1), reverse=True)
        best_10_auth_score = sorted_auth_score[0: 10]

        vertices_ids = []

        for i in range(0, len(best_10_indegree)):
            vertices_ids.append(int(best_10_indegree[i][0]))
            vertices_ids.append(int(best_10_page_rank[i][0]))
            vertices_ids.append(int(best_10_auth_score[i][0]))
        vertices_ids = set(vertices_ids)

        # confronta autorities nelle sottoreti
        self.graph_wrapper.divide_combined_net()
        g_rt = GraphWrapper(self.graph_wrapper.rt_graph)
        g_rt.graph['name'] = 'Retweet Graph'
        g_re = GraphWrapper(self.graph_wrapper.re_graph)
        g_re.graph['name'] = 'Reply Graph'
        g_mt = GraphWrapper(self.graph_wrapper.mt_graph)
        g_mt.graph['name'] = 'Mention Graph'
        subgraphs = {'rt': g_rt, 're': g_re, 'mt': g_mt}

        for g in subgraphs.values():
            g.compute_centrality_measures()
            g.compute_pagerank()
            g.compute_authority_score()
            g.compute_vertex_connectivity()
            g.compute_local_clustering_coefficient()

        self.graph_wrapper.graph['name'] = 'All Graph'
        subgraphs['all'] = self.graph_wrapper

        return subgraphs, vertices_ids

    def get_authorities(self, k=10):
        """
        Calculates the authorities for each subgraph types and returns a dictionary containing the authorities set
        for each of the following keys 'all', 'mt', 'rt', 're'.
        :param k: The number of authorities to return.
        :return: A tuple where the elements are:
            - A dictionary containing the authorities for each subgraph and for the whole graph.
            - A dictionary containing the graph wrappers used to calculate authorities, with the same keys as
            the other returned dictionary.

        """
        self.graph_wrapper.divide_combined_net()
        graphs = {'all': self.graph_wrapper, 'rt': GraphWrapper(self.graph_wrapper.rt_graph),
                  're': GraphWrapper(self.graph_wrapper.re_graph), 'mt':GraphWrapper(self.graph_wrapper.mt_graph)}

        authority_metrics_attr_names = ['pagerank', 'kl_auth', 'in_degree']
        # Costruisci un dizionario contenente per ogni grafo, un insieme rappresentante le autorità.
        authorities = defaultdict(lambda: set())
        for g_name, g in graphs.items():
            g.compute_centrality_measures()
            g.compute_pagerank()
            g.compute_authority_score()
            g.compute_vertex_connectivity()
            g.compute_local_clustering_coefficient()
            g.compute_edge_betweenness()
            g.compute_vertices_timestamp()
            for m in authority_metrics_attr_names:
                auth_values = g.vs[m]
                auth_values.sort(reverse=True)
                threshold = auth_values[k]
                for v in g.vs.select(lambda vertex: vertex[m] >= threshold):
                    authorities[g_name].add(v)
        return authorities, graphs

    def get_authorities_subgraph(self, k=10):
        """
        Calculates the top k nodes by in-degree, page rank and Kleigen authority score.
        A unique set is then created and a subgraph for each graph type ('all', 're', 'rt', 'mt') is returned.
        :param k: The number of authorities to return.
        :return: The authorities subgraph as a dictionary, with the following keys: 'all', 're', 'rt', 'mt'.
        """
        authorities, graphs = self.get_authorities(k)
        subgraphs = {}
        for k, v in authorities.items():
            subgraphs[k] = graphs[k].graph.subgraph(v)
        return subgraphs


class RSQ3(ResearchQuestions):
    def __init__(self, path):
        super(RSQ3, self).__init__(path)

    def create_pr_clusters(self):
        self.graph_wrapper.compute_pagerank()
        # todo: clustering secondo il pagerank

    def community_detection_infomap(self):
        """
        Calculates the communities in the current network and saves them as a vertex attribute.
        :return: A community graph where each vertex represents a community. Edges will be weighted based on
        the number of edges connecting each community.
        """
        g = self.graph_wrapper
        g.graph.es['weight'] = 1
        g.graph.simplify(combine_edges=dict(weight='sum'))
        g.compute_community_detection()

    def create_contract_graph(self):
        """
        Contract the vertices of the graph based on communities
        """
        g = self.graph_wrapper
        g.graph.contract_vertices(mapping=g.graph.vs['community'], combine_attrs='First')
        g.graph.simplify(combine_edges=dict(weight='sum'))

    def get_border_vertices(self, communities, mode):
        """
        Calculates the border vertices for each community pair.
        :return: The border nodes for each community as a dictionary with the community integer as key and values
        the bridge edge.
        """
        g = self.graph_wrapper
        g.compute_edge_betweenness()
        border_nodes = {}
        for community in communities:
            comm_edges = {}
            for v in g.vs.select(community=community):
                if mode == 'out':
                    incidents = g.graph.incident(v, mode)
                    incidents = [e for e in g.es[incidents] if g.graph.vs[e.target]['community'] in communities and
                                 g.graph.vs[e.target]['community'] != community]
                    for e in incidents:
                        target_community = g.graph.vs[e.target]['community']
                        if e.target == e.source:
                            continue
                        # print(e['edge_betweenness'])
                        if comm_edges.get(target_community) is None:
                            comm_edges[target_community] = e
                        elif comm_edges[target_community]['edge_betweenness'] < e['edge_betweenness']:
                                comm_edges[target_community] = e
                elif mode == 'in':
                    incidents = g.graph.incident(v, mode)
                    incidents = [e for e in g.es[incidents] if g.graph.vs[e.source]['community'] in communities and
                                 g.graph.vs[e.source]['community'] != community]
                    for e in incidents:
                        target_community = g.graph.vs[e.source]['community']
                        if e.target == e.source:
                            continue
                        # print(e['edge_betweenness'])
                        if comm_edges.get(target_community) is None:
                            comm_edges[target_community] = e
                        elif comm_edges[target_community]['edge_betweenness'] < e['edge_betweenness']:
                            comm_edges[target_community] = e

            border_nodes[community] = comm_edges
        return border_nodes


class RSQ4(ResearchQuestions):
    """
    IGNORE
    """
    def __init__(self):
        return
        super(RSQ4, self).__init__('../dataset/combined-timestamp.gml')
        self.graph_wrapper.load_followers('../dataset/followers.gml')

    def discussion_subgraph_analysis(self, start_timestamp=1341377995, start_node='88', interval=7200, cutoff=0.1, save_to_file=False):
        s_node = self.graph_wrapper.vs.find(name=start_node)
        s_edge = self.graph_wrapper.es[self.graph_wrapper.graph.incident(s_node, mode='OUT')].find(timestamp=start_timestamp)
        print("Graph stats with cutoff: ", cutoff, "\tinterval: ", interval, "\ts_node: ", start_node, "\tts: ",start_timestamp)
        print("Pre nodes: ", len(self.graph_wrapper.vs), "\tPre edges: ", len(self.graph_wrapper.es))
        s_node = self.graph_wrapper.vs[20]
        self.graph_wrapper.compute_discussion_graph(s_node, s_edge, interval, cutoff)
        print("Post nodes: ", len(self.graph_wrapper.vs.select(color='white')), "\tPost edges: ", len(self.graph_wrapper.es.select(color='white')))
        if not save_to_file:
            return
        file_path_list = ['../dataset/discussion', str(start_node), start_timestamp, interval, cutoff]
        file_path_list = [str(x) for x in file_path_list]
        self.graph_wrapper.save_to_file('-'.join(file_path_list) + '.gml')

    def delete_red_edges(self):
        e_ids = [e.index for e in self.graph_wrapper.graph.es.select(color="red")]
        self.graph_wrapper.graph.delete_edges(e_ids)
        v_ids = [v['id'] for v in self.graph_wrapper.graph.vs if v.degree() == 0]
        self.graph_wrapper.graph.delete_vertices(v_ids)

