library(igraph)
library(lsa)

activities = as.data.frame(matrix(scan("higgs-activity_time.txt", what = "character",sep = " ",encoding = "utf-8"), byrow = T, ncol=4))
colnames(activities) <- c("source", "target", "timestamp", "type")

mt_edges = as.data.frame(matrix(scan("higgs-mention_network_first.edgelist", what = "character", sep = " ",encoding = "utf-8"), byrow = T, ncol=3))
colnames(mt_edges) <- c("source", "target", "weight")
rt_edges = as.data.frame(matrix(scan("higgs-retweet_network_first.edgelist", what = "character", sep = " ",encoding = "utf-8"), byrow = T, ncol=3))
colnames(rt_edges) <- c("source", "target", "weight")
re_edges = as.data.frame(matrix(scan("higgs-reply_network_first.edgelist", what = "character", sep = "\t",encoding = "utf-8"), byrow = T, ncol=3))
colnames(re_edges) <- c("source", "target", "weight")



actMatr = matrix(scan("higgs-activity_time.txt", what = "character",sep = " ",encoding = "utf-8"), byrow = T, ncol=4)
actGraph = graph_from_edgelist(actMatr[,1:2])
E(actGraph)$timestamp = actMatr[,3]
E(actGraph)$type = actMatr[,4]

mtMatr = matrix(scan("higgs-mention_network_first.edgelist", what = "character", sep = " ",encoding = "utf-8"), byrow = T, ncol=3)
mtGraph = graph_from_edgelist(mtMatr[,1:2])
E(mtGraph)$type = "MT"
rtMatr = matrix(scan("higgs-retweet_network_first.edgelist", what = "character", sep = " ",encoding = "utf-8"), byrow = T, ncol=3)
rtGraph = graph_from_edgelist(rtMatr[,1:2])
E(rtGraph)$type = "RT"
reMatr = matrix(scan("higgs-reply_network_first.edgelist", what = "character", sep = "\t",encoding = "utf-8"), byrow = T, ncol=3)
reGraph = graph_from_edgelist(reMatr[,1:2])
E(reGraph)$type = "RE"


tmpGraph <- subgraph.edges(actGraph, E(actGraph)[type == "MT"])
enrichMtGraph <- subgraph.edges(tmpGraph, E(tmpGraph)[E(tmpGraph) %in% E(mtGraph)])

tmpGraph <- subgraph.edges(actGraph, E(actGraph)[type == "RE"])
enrichReGraph <- subgraph.edges(tmpGraph, E(tmpGraph)[E(tmpGraph) %in% E(reGraph)])

tmpGraph <- subgraph.edges(actGraph, E(actGraph)[type == "RT"])
enrichRtGraph <- subgraph.edges(tmpGraph, E(tmpGraph)[E(tmpGraph) %in% E(rtGraph)])

write_graph(enrichMtGraph, file = "enrichedMtGraph.edgelist", format = "ncol")
write_graph(enrichRtGraph, file = "enrichedRtGraph.edgelist", format = "edgelist")
write_graph(enrichReGraph, file = "enrichedReGraph.edgelist", format = "edgelist")
