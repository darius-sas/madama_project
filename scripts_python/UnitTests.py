import unittest as ut
from scripts_python.ResearchQuestions import *


class RQ1Test(ut.TestCase):
    def setUp(self):
        self.rq = RSQ1('../dataset/combined-timestamp.gml')
        #self.rq.compute_metrics()
        self.test_plotting = False

    def test_get_tweets_frequency(self):
        ts, freq = self.rq.get_tweets_frequency()
        self.assertEqual(len(set(ts)), len(ts), "Timestamps should be unique.")
        self.assertTrue(len(freq['all']) == len(freq['rt']))
        self.assertTrue(len(freq['all']) == len(freq['re']))
        self.assertTrue(len(freq['all']) == len(freq['mt']))

    def test_get_cumulative_tweets(self):
        ts, freq = self.rq.get_cumulative_tweets()
        self.assertEqual(len(set(ts)), len(ts), "Timestamps should be unique.")
        for k, v in freq.items():
            for index, item in enumerate(v):
                if index - 1 < 0:
                    continue
                self.assertTrue(v[index] >= v[index - 1],
                                "Cumulated tweets in '"+ k + "', at index '" + str(index) + "' should not decrease at each step.")

    def test_get_highest_activity_timestamps(self):
        # Tweets del 4/7
        ts = self.rq.get_highest_activity_timestamps(timestamp_start=1341360000, timestamp_end=1341446400)

        # I massimi devono essere tra le 7 del mattino e le 20 di sera
        for e in ts:
            self.assertTrue(1341378000 <= e < 1341432000)

        ts = self.rq.get_highest_activity_timestamps()

        # I massimi devono essere tra le 7 del mattino e le 20 di sera
        for e in ts:
            self.assertTrue(1341378000 <= e < 1341432000)

    def test_plot_tweets_per_second(self):
        if self.test_plotting:
            self.rq.plot_cumulative_tweets()

    def test_plot_tweets_per_second_percentage(self):
        if self.test_plotting:
            self.rq.plot_tweets_per_sec_percentage(['all', 'rt', 're', 'mt'])

    def test_plot_heatmap(self):
        if self.test_plotting:
            self.rq.plot_tweets_per_second_heatmap()


class RQ2Test(ut.TestCase):
    def setUp(self):
        self.rq = RSQ2('../dataset/combined-timestamp.gml')

    def test_get_authorities(self):
        auth, graphs = self.rq.get_authorities(10)
        unique_nodes = set()
        for k, s in auth.items():
            for v in s:
                unique_nodes.add(v) # TODO: Set non mantiene l'univocità in caso un vertice abbia un attributo con valore diverso!
        print(unique_nodes)
        self.assertTrue(len(unique_nodes) >= 10)


def main():
    ut.main()

if __name__ == '__main__':
    main()