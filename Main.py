from scripts_python.Analysis import *
from scripts_python.GraphWrapper import *
from scripts_python.ResearchQuestions import *

# CREAZIONE GRAFI CON MISURE CALCOLATE DA PYTHON
# g = GraphWrapper()
# g.load_gml('../dataset/grafo_con_communities_infomap_aggregated.gml')
# subg = g.subgraph_by_communities([0, 1, 2, 4, 8, 9])
# g.compute_centrality_measures()
# g.compute_pagerank()
# g.compute_authority_score()
# g.compute_vertex_connectivity()
# g.compute_local_clustering_coefficient()
# g.load_followers('../dataset/followers.gml')
# g.save_to_file('../relazione/dati/network_with_measures.gml')

# ISTOGRAMMI DEI GRADI PER LE RETI
# filenames = ['../dataset/combined-timestamp.gml','../dataset/rt_combined.gml','../dataset/mt_combined.gml','../dataset/re_combined.gml']
# for file in filenames:
#     g = GraphWrapper()
#     g.load_gml(file)
#     plt = Plot()
#     plt.plot_histogram(file[11:13], [0,100], [0, 11000], [g.graph.degree(mode='IN'),'IN degree'], [g.graph.degree(mode='OUT'), 'OUT degree'])

# NETWORK GRAPH
# plt = Plot()
# plt.plot_network_graph(subg)
rq = RSQ1('./dataset/combined-timestamp.gml')
rq.plot_tweets_per_second_heatmap()

exit()

# rq = RSQ3()
# rq.infomap_community_detection()
# borders = rq.get_border_vertices(
# [0, 1, 2, 4, 8, 9])
# for com in borders.keys():
#     print('Community ', com, ': ')
#     for target in borders[com].keys():
#         print('interact with: ', target,
#               'from ', rq.graph_wrapper.graph.vs[borders[com][target].source]['name'],
#               'to ', rq.graph_wrapper.graph.vs[borders[com][target].target]['name'],
#               'eb ', borders[com][target]['edge_betweenness'])

# rq.discussion_subgraph_analysis(cutoff=0.5, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.25, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.1, save_to_file=True)
# rq.discussion_subgraph_analysis(cutoff=0.05, save_to_file=True)
